from django.db import models
from django.contrib.auth.models import User
from datetime import datetime

from django.conf import settings


ESTADO_CHOICES = (
        ('PORVALIDAR', 'Por Validar'),
        ('VALIDADO', 'validado'),
        ('RESOLVIDO', 'resolvido')
    )

# Create your models here.
class Categoria(models.Model):
    id = models.AutoField(primary_key=True)
    nome = models.CharField(max_length=128,  unique=True)
    descricao = models.CharField(max_length=1024)
    def __str__(self):
        return self.descricao


class Ocorrencias(models.Model):
    id = models.AutoField(primary_key=True)
    descricao = models.CharField(max_length=1024)
    autor = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE)
    data_criacao = models.DateTimeField(default=datetime.now, blank=True)           #considera-se que a data de criação é a data de submissão do formulário
    estado = models.CharField(max_length=16, choices=ESTADO_CHOICES, default="PORVALIDAR")
    categoria = models.ForeignKey(Categoria,on_delete=models.CASCADE)
    def __str__(self):
        return self.descricao