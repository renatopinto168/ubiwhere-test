from rest_framework import serializers

from .models import Ocorrencias, Categoria
from django.contrib.auth.models import User


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id','username','email')

class UserSerializerLite(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('id',)


class CategoriaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Categoria
        fields = ('id','nome', 'descricao')


class CategoriaSerializerLite(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Categoria
        fields = ('id',)


class OcorrenciaSerializerCreate(serializers.HyperlinkedModelSerializer):
    autor = UserSerializerLite(read_only=False)
    categoria = CategoriaSerializerLite(read_only=False)
    class Meta:
        model = Ocorrencias
        depth = 1
        fields = ('descricao', 'autor','categoria')

    @classmethod
    def create(self, validated_data):
        #não consegui ir buscar os valores dos id's do autor e da categoria
        thisautor = User.objects.get(id=1)
        thiscategoria = Categoria.objects.get(id=18)
        thisdescricao = validated_data.pop('descricao')
        newOcorrencia = Ocorrencias.objects.create(autor=thisautor, categoria=thiscategoria, descricao=thisdescricao)
        return newOcorrencia

  

class OcorrenciaSerializer(serializers.HyperlinkedModelSerializer):
    autor = UserSerializerLite()
    categoria = CategoriaSerializerLite()
    class Meta:
        model = Ocorrencias
        depth = 1
        fields = ('id','descricao', 'autor', 'data_criacao', 'estado', 'categoria')

class OcorrenciaSerializerEstado(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Ocorrencias
        depth = 1
        fields = ('estado',)

    @classmethod
    def update(self, request, pk):
        ocorrencia_model = Ocorrencias.objects.get(id=pk)
        pass