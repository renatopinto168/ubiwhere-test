from django.conf.urls import url
from app import views

urlpatterns = [
    url(r'^api/ubiwhere/Ocorrencias', views.ocorrencias_list),
    url(r'^api/ubiwhere/Ocorrencias/(?P<pk>[0-9]+)$', views.ocorrencias_info),
    url(r'^api/ubiwhere/Users', views.users),
    url(r'^api/ubiwhere/Users/(?P<pk>[0-9]+)$', views.users_info),
    url(r'^api/ubiwhere/Categorias', views.categorias),
    url(r'^api/ubiwhere/Categorias/(?P<pk>[0-9]+)$', views.categorias_info),
    url(r'^api/ubiwhere/Estado/Ocorrencia/(?P<pk>[0-9]+)$', views.estado_ocorrencia),
]