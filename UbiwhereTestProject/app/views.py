"""
Definition of views.
"""

from datetime import datetime
from django.shortcuts import render
from django.http import HttpRequest

from rest_framework import viewsets
from .serializer import OcorrenciaSerializer,OcorrenciaSerializerCreate,UserSerializer,CategoriaSerializer,OcorrenciaSerializerEstado

from .models import Ocorrencias, Categoria
from .models import User

from django.http.response import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.parsers import JSONParser

#Todas as views API devem de ter um @permission_classes([permissions.IsAuthenticated]) no entanto não foi implementado pois não consegui a autenticação no postman

@api_view(['GET', 'PUT'])
@permission_classes((AllowAny, ))
def users(request):
    if request.method == 'GET':
        queryset = User.objects.all().order_by('id')
        users_serializer = UserSerializer(queryset, many=True)
        return JsonResponse(users_serializer.data, safe=False)
    elif request.method == 'PUT':
        user_data = JSONParser().parse(request) 
        user_serializer = UserSerializer(data=user_data) 
        if user_serializer.is_valid(): 
            user_serializer.save() 
            return JsonResponse(user_serializer.data) 
        return JsonResponse(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
    
@api_view(['GET'])
@permission_classes((AllowAny, ))
def users_info(request,pk):
    if request.method == 'GET':
        queryset = User.objects.get(id=pk) 
        users_serializer = UserSerializer(queryset, many=False)
        return JsonResponse(users_serializer.data, safe=False)
   

@api_view(['GET', 'DELETE', 'POST'])
@permission_classes((AllowAny, ))
def ocorrencias_list(request):
    if request.method == 'GET':
        queryset = Ocorrencias.objects.all().order_by('descricao')
        ocorrencias_serializer = OcorrenciaSerializer(queryset, many=True)
        return JsonResponse(ocorrencias_serializer.data, safe=False)
    elif request.method == 'DELETE':
        count = Ocorrencias.objects.all().delete()
        return JsonResponse({'message': '{} Ocorrencias foram apagadas com sucesso!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)
    elif request.method == 'POST':
        ocorrencias_data = JSONParser().parse(request)
        ocorrencias_serializer = OcorrenciaSerializerCreate(data=ocorrencias_data)
        if ocorrencias_serializer.is_valid():
            ocorrencias_serializer.save()
            return JsonResponse(ocorrencias_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(ocorrencias_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

def ocorrencias_info(request,pk):
    if request.method == 'GET':
        queryset = Ocorrencias.objects.get(id=pk) 
        ocorrencias_serializer = OcorrenciaSerializer(queryset, many=False)
        return JsonResponse(ocorrencias_serializer.data, safe=False)

@api_view(['GET','POST', 'DELETE'])
@permission_classes((AllowAny, ))
def categorias(request):
    if request.method == 'GET':
        queryset = Categoria.objects.all().order_by('id')
        categoria_serializer = CategoriaSerializer(queryset, many=True)
        return JsonResponse(categoria_serializer.data, safe=False)
    elif request.method == 'POST':
        categoria_data = JSONParser().parse(request) 
        categoria_serializer = CategoriaSerializer(data=categoria_data) 
        if categoria_serializer.is_valid(): 
            categoria_serializer.save() 
            return JsonResponse(categoria_serializer.data) 
        return JsonResponse(categoria_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
    elif request.method == 'DELETE':
        count = Categoria.objects.all().delete()
        return JsonResponse({'message': '{} Categorias foram apagadas com sucesso!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)

@api_view(['GET'])
def categorias_info(request,pk):
    if request.method == 'GET':
        queryset = Categoria.objects.get(id=pk) 
        ccategoria_serializer = CategoriaSerializer(queryset, many=False)
        return JsonResponse(ccategoria_serializer.data, safe=False)


@api_view(['PUT'])
@permission_classes((AllowAny, ))
def estado_ocorrencia(request,pk):
    if request.method == 'PUT':
        ocorrencias_data = JSONParser().parse(request)
        ocorrencias_serializer = OcorrenciaSerializerEstado(data=ocorrencias_data)
        if ocorrencias_serializer.is_valid():
            ocorrencias_serializer.update(ocorrencias_data,pk)
            return JsonResponse(ocorrencias_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(ocorrencias_serializer.errors, status=status.HTTP_400_BAD_REQUEST)



### django defaults ###
def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )
